<!--

Copyright (C) 2017 Francisco Domínguez Lerma

 	Author: Francisco Domínguez Lerma 

This file is part of Privatekeys.

    Privatekeys is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Privatekeys is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with privatekeys.  If not, see <http://www.gnu.org/licenses/>.
 -->

<!DOCTYPE html>

<html>
<head>
<title>privatekeys</title>
<meta http-equiv="Refresh" content="5;url=index.html">
<link rel="stylesheet" href="../css/estilos.css">
<link rel="shortcut icon" href="../favicon.png">
</head>
<body>

<?php

if((!$user_bd=$_POST['user_bd']) or (!$pass_bd=$_POST['pass_bd']) or (!$name_bd=$_POST['name_bd']) or (!$pass_registration=$_POST['pass_registration']) or (!$pass_cifrado=$_POST['pass_cifrado'])) {
die("<h1>Por favor rellene todos los campos</h1>");
}

$pass_registration=sha1($pass_registration);


if(!file_exists("/etc/privatekeys/privatekeys.ini")) {} else {

die("<h1>Instalación existente detectada</h1>");
}

fopen("/etc/privatekeys/privatekeys.ini", "w");

if(!file_exists("/etc/privatekeys/privatekeys.ini")) {

die("<h1>No se ha podido crear fichero /etc/privatekeys/privatekeys.ini</h1>");
}


$conexion=mysql_connect("localhost", $user_bd, $pass_bd);
if(!$conexion) {
unlink("/etc/privatekeys/privatekeys.ini");
die("<h1>No se pudo conectar con la base de datos</h1>");

}

$bd=mysql_select_db($name_bd, $conexion);

if(!$bd) {
unlink("/etc/privatekeys/privatekeys.ini");
die("<h1>Problemas seleccionando la base de datos</h1>");

}

$crear_tabla="create table users (nombre VARCHAR(30),clave VARCHAR(100), tabla VARCHAR(35))";
$crear_tabla_consulta=mysql_query($crear_tabla, $conexion);

if(!$crear_tabla_consulta) {
unlink("/etc/privatekeys/privatekeys.ini");
die("<h1>Error creando tabla de usuarios</h1>");

}

$archivo=fopen("/etc/privatekeys/privatekeys.ini", "a");

fputs($archivo, "user_bd = $user_bd\n");
fputs($archivo, "pass_bd = $pass_bd\n");
fputs($archivo, "name_bd = $name_bd\n");
fputs($archivo, "pass_registration = $pass_registration\n");
fputs($archivo, "pass_cifrado = $pass_cifrado\n");

fclose($archivo);
echo "<h1>¡Instalación de privatekeys completada!</h1>";
echo "<h2>No olvide borrar la carpeta /install</h2>";
?>
</body>
</html>
