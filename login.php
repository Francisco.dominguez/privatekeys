<!--

Copyright (C) 2017 Francisco Domínguez Lerma

 	Author: Francisco Domínguez Lerma 

This file is part of Privatekeys.

    Privatekeys is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Privatekeys is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with privatekeys.  If not, see <http://www.gnu.org/licenses/>.
 -->

<!DOCTYPE html>
<html>
<head>
<title>privatekeys</title>
<meta http-equiv="Refresh" content="10;url=index.html">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/estilos.css">
<link rel="shortcut icon" href="favicon.png">
</head>

<body>
<?php

if(!fopen("/etc/privatekeys/privatekeys.ini", "r")) {

echo "<h1>Archivo .ini no accesible</h1>";
die("<h2>Probablemente no ha instalado correctamente privatekeys, acceda desde el navegador a /install para iniciar el proceso de instalación</h2>");


}

$array_ini = parse_ini_file("/etc/privatekeys/privatekeys.ini");

if(!$nombre=$_POST['nombre']) {die("<h1>Tienes que escribir un nombre de usuario</h1>");}
if(!$_POST['clave']) {die("<h1>Tienes que escribir una clave</h1>");}
$clave=sha1($_POST['clave']);

$conexion=mysql_connect("localhost", $array_ini['user_bd'], $array_ini['pass_bd'])
	or die("no se ha podido conectar con el servidor");
mysql_select_db($array_ini['name_bd'], $conexion) or die("Problemas seleccionando base de datos");

$consultaa=sprintf("SELECT * FROM users WHERE nombre='%s'", mysql_real_escape_string($nombre));
$consulta=mysql_query($consultaa, $conexion) or die("problema en un select");
$fila=mysql_fetch_assoc($consulta);

$consultaa2=sprintf("select clave from users where nombre='%s'", mysql_real_escape_string($nombre));
$consulta2=mysql_query($consultaa2) or die("problemas en un select");
$array_clave=mysql_fetch_array($consulta2);
$clave_real=$array_clave[0];

if ($clave==$clave_real) {
	session_start();
	$_SESSION['user']=$fila['nombre'];
	header("Location: privatekeys.php");
	
	
} else {
echo "<h1>Clave incorrecta</h1>";
}

?>

</body>
</html>
