<!--

Copyright (C) 2017 Francisco Domínguez Lerma

 	Author: Francisco Domínguez Lerma 

This file is part of Privatekeys.

    Privatekeys is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Privatekeys is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with privatekeys.  If not, see <http://www.gnu.org/licenses/>.
 -->

<!DOCTYPE html>
<html>
<head>
<title>privatekeys</title>
<meta http-equiv="Refresh" content="5;url=index.html">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/estilos.css">
<link rel="shortcut icon" href="favicon.png">
</head>

<body>

<?php
session_start();
session_unset();
session_destroy();
echo "<h1>Has cerrado sesión en privatekeys</h1>";
echo "<p>Redirigiendo a la página de login</p>";



?>


</body>
</html>
