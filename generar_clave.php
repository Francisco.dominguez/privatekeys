<!--

Copyright (C) 2017 Francisco Domínguez Lerma

 	Author: Francisco Domínguez Lerma 

This file is part of Privatekeys.

    Privatekeys is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Privatekeys is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with privatekeys.  If not, see <http://www.gnu.org/licenses/>.
 -->

<!DOCTYPE html>

<html>
<head>
<title>privatekeys</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/estilos.css">
<link rel="shortcut icon" href="favicon.png">
</head>
<body>

<?php
echo "<h1>Generando entropía</h1>";
system("du -s>/dev/null");
echo "<h1>Listado de claves, elige la que más te guste</h1>";

for ($i = 15; $i <= 50; $i++) {
    echo "<p>-------------------</p>";
    system("gpg --gen-random --armor 0 ".$i);
    echo "<p>-------------------</p>";
}
?>
</body>
</html>
