<!--

Copyright (C) 2017 Francisco Domínguez Lerma

 	Author: Francisco Domínguez Lerma 

This file is part of Privatekeys.

    Privatekeys is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Privatekeys is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with privatekeys.  If not, see <http://www.gnu.org/licenses/>.
 -->

<!DOCTYPE html>

<html>
<head>
<title>privatekeys</title>
<meta http-equiv="Refresh" content="5;url=registro.html">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/estilos.css">
<link rel="shortcut icon" href="favicon.png">
</head>
<body>

<?php

$nombre=$_POST['nombre'];
$clave=$_POST['clave'];
$clave_admin=$_POST['pass_registration'];
$clave_admin=sha1($clave_admin);

$array_ini = parse_ini_file("/etc/privatekeys/privatekeys.ini");


$nombretabla=sprintf("tabla%s", $nombre);

if ($clave_admin != $array_ini['pass_registration']) {

	die("<h1>Clave de administracion incorrecta</h1>");

	}

$conexion=mysql_connect("localhost", $array_ini['user_bd'], $array_ini['pass_bd'])
	or die("no se ha podido conectar con el servidor");
mysql_select_db($array_ini['name_bd'], $conexion) or die("<h1>Problemas seleccionando base de datos</h1>");

$comprobar_si_existe=mysql_query("select * from users where nombre=\"$nombre\"");
$comprobar=mysql_fetch_array($comprobar_si_existe);

if ($comprobar) {

	die("<h1>El usuario ya existe en la base de datos</h1>");

	}


$consulta=sprintf("insert into users values(\"%s\", sha1('%s'), \"%s\")", mysql_real_escape_string($nombre), mysql_real_escape_string($clave), mysql_real_escape_string($nombretabla));
$consulta=mysql_query($consulta, $conexion) or die("problemas creando el usuario en la base de datos");

$consulta2=sprintf("create table %s(servicio varchar(20), clavet varchar(100))", mysql_real_escape_string($nombretabla));
$consulta2=mysql_query($consulta2, $conexion) or die("problemas creando la tabla del usuario");

echo "<h1>El usuario $nombre se ha agregado con éxito a la base de datos</h1>";
?>
</body>
</html>
